package utils

import (
	"os"
	"strings"
)

func GetOrDefault(m map[string]string, k string, d string) string {
	if v, ok := m[k]; ok {
		return v
	}
	return d
}

func GetOrDie(m map[string]string, k string) string {
	v := GetOrDefault(m, k, "")
	if v == "" {
		panic("required key not found: " + k)
	}
	return v
}

func GetEnvOrDefault(k string, d string) string {
	return GetOrDefault(GetEnv(), k, d)
}

func GetEnvOrDie(key string) string {
	return GetOrDie(GetEnv(), key)
}

func GetEnv() map[string]string {
	m := make(map[string]string)
	for _, e := range os.Environ() {
		pair := strings.Split(e, "=")
		m[pair[0]] = pair[1]
	}
	return m
}
